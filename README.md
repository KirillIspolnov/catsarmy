# CatsArmy
### Армия котиков
Котики предельно опасны! Они умеют:
* Мяукать
* Гулять по улице
* Кушать
При выполнении этих действий голод котика постепенно увеличивается. Не забывайте его кормить!

Каждый котик обладает свойствами:
* Год рождения
* Кличка
* Пол
* Голод

### Репозиторий создан в рамках прохождения обучения в [Пензенском колледже информационных и промышленных технологий (ИТ-колледж)](https://ит-колледж.рф)
### Автор: [Кирилл Испольнов](https://vk.com/kivi_penza)