package net.kivitechnologies.BrainWork.CatsArmy;

/**
 * Класс, представляющий еду для животных
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Food {
    /* Максимально возможная сытность еды */
    public static final int MAX_RICHNESS = 1000;
    
    /* Текущая сытность еды */
    private int currentRichness;
    
    /**
     * Создает объект с заданной сытностью
     * 
     * @param richness сытность еды. Если показатель больше MAX_RICHNESS, то как сытность еды будет использован отсаток от деления richness % MAX_RICHNESS
     */
    public Food(int richness) {
        this.currentRichness = richness % MAX_RICHNESS;
    }
    
    /**
     * Возварадщает текущую сытность еды
     * 
     * @return сытность
     */
    public int getCurrentRichness() {
        return  currentRichness;
    } 
    
    /**
     * Уменьшает сытность еды в связи с поеданием ее животным
     * 
     * @param hungry голод животного
     */
    public int use(int hungry) {
        hungry = Math.min(hungry, currentRichness);
        currentRichness -= hungry;
        return hungry;
    }
}
