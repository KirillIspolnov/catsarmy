package net.kivitechnologies.BrainWork.CatsArmy;

/**
 * Основной класс
 * Демонстрирует работу класса Cat
 * 
 * @author �?спольнов Кирилл, 16ит18К
 */
public class Main {
    //Текущий год - 2017
    public static final int CURRENT_YEAR = 2017;
    
    /**
     * Основной метод
     * 
     * @param args Массив строковых аргументов, переданных в терминале
     */
    public static void main(String[] args) {
        Cat[] heapOfCats = new Cat[16];
        
        createCats(heapOfCats);
        
        int indexOfOldestCat = findOldestCat(heapOfCats);
        Cat oldestCat = heapOfCats[indexOfOldestCat];
        System.out.printf("Самый старый кот - %s, ему %d лет и он голоден на %d%n", oldestCat.getName(), oldestCat.getAge(CURRENT_YEAR), oldestCat.getHungry());
        
        Cat myCat = new Cat("Елизавета II", false, 2015);
        myCat.meow();
        myCat.meow();
        myCat.meow();
        myCat.walk();
        myCat.eat(new Food(250));
        System.out.println(myCat);
    }  

    /**
     * �?нициализирует массив котиков в цикле случайо созданными котиками
     * 
     * @param cats массив котиков для инициализации
     */
    private static void createCats(Cat[] cats) {
        for(int i = 0; i < cats.length; i++) {
            cats[i] = new Cat("Котэ №" + (i + 1), (int)(Math.random() * 17 + 2000));
        }
    }
    
    /**
     * Находит в массиве котиков самого старого кота или кошку
     * 
     * @param cats массив котиков
     * @return номер котика в массиве
     */
    public static int findOldestCat(Cat[] cats) {
        int oldestCatIndex = 0;
        for(int i = 1; i < cats.length; i++) {
            if(cats[i].getAge(CURRENT_YEAR) > cats[oldestCatIndex].getAge(CURRENT_YEAR)) {
                oldestCatIndex = i;
            }
        }
        
        return oldestCatIndex;
    }
}
