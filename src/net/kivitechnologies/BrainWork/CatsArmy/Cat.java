package net.kivitechnologies.BrainWork.CatsArmy;

/**
 * Класс, предствавляющий кота в довольно примитивном виде
 * Кот обладает неоторыми свойствами:
 * <ul>
 * <li>Кличка</li>
 * <li>Год рождения</li>
 * <li>Пол</li>
 * <li>Голод</li>
 * </ul>
 * 
 * Также кот умеет:
 * <ul>
 * <li>Мяукать</li>
 * <li>Кушать</li>
 * <li>Гулять</li>
 * </ul>
 * 
 * При выполнении этих действий кот по немногу голодает 
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Cat {
    //Год рорждения
    private int birthYear;
    //Кличка
    private String name;
    //Голод
    private int hungry;
    //Является ли самцом
    private boolean male;
    
    /**
     * Основной конструктор
     * Изначально кот сыт
     * 
     * @param name Кличка
     * @param male Является ли самцом
     * @param birthYear год рождения
     */
    public Cat(String name, boolean male, int birthYear) {
        this.name = name;
        this.male = male;
        this.birthYear = birthYear;
        this.hungry = 0;
        
        System.out.printf("Существует %s по кличке %s родил%s в %d-ном году%n", getCorrectPronoun(), name, male ? "ся" : "ась", birthYear);
    }
    
    /**
     * Конструктор, игнорирующий пол
     * В этом случае пол котика устанавливается случайно
     * 
     * @param name Клчика
     * @param birthYear год рождения
     */
    public Cat(String name, int birthYear) {
        this(name, Math.random() < 0.5, birthYear);
    }
    
    /**
     * Конструктор, игнорирующий пол и год рождения
     * В этом случае пол задается случайно
     * А год рождения - текущий год, хранящийся в константе класса Main
     *  
     * @param name Кличка
     */
    public Cat(String name) {
        this(name, Main.CURRENT_YEAR);
    }
    
    /**
     * Возвращает кличку котика
     * 
     * @return кличка котика
     */
    public String getName() {
        return this.name;
    }
    
    public int getHungry() {
        return this.hungry;
    }
    
    /**
     * Возвращает год рождения кота
     * 
     * @return год рождения
     */
    public int getBirthYear() {
        return this.birthYear;
    }
    
    /**
     * Возвращает возраст кота
     * 
     * @param year год, в котором коту было result лет
     * @return result возраст кота в year год или 0, елси кота еще не было
     */
    public int getAge(int year) {
        int age = year - this.birthYear;
        return age <= 0 ? 0 : age; 
    }
    
    /**
     * Мяукательный метод
     */
    public void meow() {
        System.out.printf("%s по кличке %s передает привет: МЯУ!%n", getCorrectPronoun(), this.name);
    }

    /**
     * Кушательный метод
     * Снижает уровень голода за счет еды food
     * 
     * @param food еда для котика
     */
    public void eat(Food food) {
        if(food.getCurrentRichness() > 0) {
            say("Ом-ном-ном");
            hungry -= food.use(hungry);
            if(hungry == 0) {
                say("Ой как вкусно! Я наелся!");
            } else {
                say("Вкусненько! А где еще?");
            }
        } else {
            say("Ой! Тут пусто! Где кушать? Сейчас как кусь!");
        }
    }
    
    /**
     * Гулятельный метод
     * Во время прогулки кот очень сильно модет проголодаться
     * Не забудьте его покормить!
     */
    public void walk() {
        say("Я иду гулять!");
        if(Math.random() < 0.5) {
            say("Но там же холодно!");
            hungry += 350;
        } else {
            say("Ураааааа!");
            hungry += 200;
        }
    }  
    
    @Override
    public String toString() {
        return getCorrectPronoun() + " по кличке " + this.name + " живет с " + this.birthYear + " года и голодный на " + this.hungry + " / 1000"; 
    }
    
    /**
     * Выводит сообшение message после вывода клички котика
     * 
     * @param message сообщения
     */
    private void say(String message) {
        System.out.printf("%s %s: %s\n", getCorrectPronoun(), this.name, message);
        hungry++;
    }
    
    /**
     * Возвращет корректоное написание кот или кошка в зависимсости от пола котика
     * 
     * @return строка "Кот", если объект представляет самца, иначе - "Кошка"
     */
    private String getCorrectPronoun() {
        return male ? "Кот" : "Кошка";
    }
}